import json
import threading
import time

import pika

from publish import BatchViews

print('SAC-SERVICE-BATCH')
connect = pika.BlockingConnection(
    pika.ConnectionParameters(host='rabbitmq',
                              port=5672,
                              credentials=pika.credentials.PlainCredentials('guest', 'guest')))
channel = connect.channel()

EXCHANGE_LOG = 'log'
ROUTING_CONFIG = 'config'
QUEUE_LOG_CONFIG = f'{EXCHANGE_LOG}.logConfigQueue'
ROUTING_CONFIG_DETAIL = 'configDetail'
QUEUE_LOG_CONFIG_DETAIL = f'{EXCHANGE_LOG}.logConfigDetailQueue'

EXCHANGE_CONFIG = 'config'
ROUTING_DATA = 'data'
QUEUE_CONFIG_DATA = f'{EXCHANGE_CONFIG}.configDataQueue'

EXCHANGE_DLX = 'DLX'
ROUTING_DLX_LOG_CONFIG = QUEUE_LOG_CONFIG
QUEUE_DLX_LOG_CONFIG = f'{QUEUE_LOG_CONFIG}.dlq'
ROUTING_DLX_LOG_CONFIG_DETAIL = QUEUE_LOG_CONFIG_DETAIL
QUEUE_DLX_LOG_CONFIG_DETAIL = f'{QUEUE_LOG_CONFIG_DETAIL}.dlq'
ROUTING_DLX_DATA_CONFIG = QUEUE_CONFIG_DATA
QUEUE_DLX_DATA_CONFIG = f'{QUEUE_CONFIG_DATA}.dlq'

###BIND DLX log.logConfigQueue
channel.queue_declare(queue=QUEUE_DLX_LOG_CONFIG, durable=True)
channel.exchange_declare(exchange=EXCHANGE_DLX, exchange_type='direct', durable=True)
channel.queue_bind(queue=QUEUE_DLX_LOG_CONFIG, exchange=EXCHANGE_DLX, routing_key=ROUTING_DLX_LOG_CONFIG)

###BIND log.logConfigQueue
channel.queue_declare(queue=QUEUE_LOG_CONFIG, durable=True,
                      arguments={'x-dead-letter-exchange': EXCHANGE_DLX,
                                 'x-dead-letter-routing-key': QUEUE_LOG_CONFIG})
channel.exchange_declare(exchange=EXCHANGE_LOG, exchange_type='topic', durable=True)
channel.queue_bind(queue=QUEUE_LOG_CONFIG, exchange=EXCHANGE_LOG, routing_key=ROUTING_CONFIG)

###BIND DLX log.logConfigDetailQueue
channel.queue_declare(queue=QUEUE_DLX_LOG_CONFIG_DETAIL, durable=True)
channel.exchange_declare(exchange=EXCHANGE_DLX, exchange_type='direct', durable=True)
channel.queue_bind(queue=QUEUE_DLX_LOG_CONFIG_DETAIL, exchange=EXCHANGE_DLX, routing_key=ROUTING_DLX_LOG_CONFIG_DETAIL)

###BIND log.logConfigDetailQueue
channel.queue_declare(queue=QUEUE_LOG_CONFIG_DETAIL, durable=True,
                      arguments={'x-dead-letter-exchange': EXCHANGE_DLX,
                                 'x-dead-letter-routing-key': QUEUE_LOG_CONFIG_DETAIL})
channel.exchange_declare(exchange=EXCHANGE_LOG, exchange_type='topic', durable=True)
channel.queue_bind(queue=QUEUE_LOG_CONFIG_DETAIL, exchange=EXCHANGE_LOG, routing_key=ROUTING_CONFIG_DETAIL)

###BIND DLX log.configDataQueue
channel.queue_declare(queue=QUEUE_DLX_DATA_CONFIG, durable=True)
channel.exchange_declare(exchange=EXCHANGE_DLX, exchange_type='direct', durable=True)
channel.queue_bind(queue=QUEUE_DLX_DATA_CONFIG, exchange=EXCHANGE_DLX, routing_key=ROUTING_DLX_DATA_CONFIG)

###BIND config.configDataQueue
channel.queue_declare(queue=QUEUE_CONFIG_DATA, durable=True,
                      arguments={'x-dead-letter-exchange': EXCHANGE_DLX,
                                 'x-dead-letter-routing-key': QUEUE_CONFIG_DATA})
channel.exchange_declare(exchange=EXCHANGE_CONFIG, exchange_type='topic', durable=True)
channel.queue_bind(queue=QUEUE_CONFIG_DATA, exchange=EXCHANGE_CONFIG, routing_key=ROUTING_DATA)
channel.close()

THREADS = 5


class ThreadedConsumer(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host='rabbitmq',
                port=5672,
                credentials=pika.credentials.PlainCredentials('guest', 'guest')))
        self.channel = connection.channel()
        self.channel.basic_qos(prefetch_count=THREADS * 10)
        self.channel.basic_consume(queue=QUEUE_CONFIG_DATA, on_message_callback=self.callback)
        threading.Thread(target=self.channel.basic_consume(queue=QUEUE_CONFIG_DATA, on_message_callback=self.callback))

    def callback(self, channel, method, properties, body):
        message = json.loads(body)
        time.sleep(2)
        print(message)
        batch = BatchViews()
        if message['type'] == 0 and message['type_req'] == 4:
            batch.create(message)
        elif message['type'] == 0 and message['type_req'] == 0:
            batch.check(message)
        elif message['type'] == 1 and message['type_req'] == 1:
            batch.check_unreg_SSH(message)
        elif message['type'] == 1 and message['type_req'] == 2:
            batch.check_reg_SSH(message)
        elif message['type'] == 1 and message['type_req'] == 3:
            batch.check_service_SSH(message)
        channel.basic_ack(delivery_tag=method.delivery_tag)

    def run(self):
        print('starting thread to consume from rabbit...')
        self.channel.start_consuming()


def main():
    for i in range(THREADS):
        print('launch thread', i)
        td = ThreadedConsumer()
        td.start()


main()