FROM microservices:python
ENV PYTHONUNBUFFERED=1
RUN mkdir /opt/sac-service-batch
COPY . /opt/sac-service-batch
WORKDIR /opt/sac-service-batch
RUN pip install --no-cache-dir -r requirements.txt
EXPOSE 8003
ENTRYPOINT ["python", "main.py"]
